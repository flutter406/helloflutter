import 'package:flutter/material.dart';

void main(){
  //safearea
  runApp(HelloFlutterApp());
}
class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String thaiGreeting = "สวัสดี Flutter";
String japanGreeting = "Ohayo Flutter";
class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displaText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(onPressed:(){
              setState(() {
                displaText = displaText == englishGreeting?
                spanishGreeting:englishGreeting;
              });
            },
                icon: Icon(Icons.flag_circle_rounded)),
            IconButton(onPressed: () {
              setState(() {
                displaText = displaText == englishGreeting?
                thaiGreeting:englishGreeting;
              });
            },
              icon: Icon(Icons.flag_circle_outlined)),
            IconButton(onPressed: () {
              setState(() {
                displaText = displaText == englishGreeting?
                japanGreeting:englishGreeting;
              });
            },
                icon: Icon(Icons.flag)),
          ],
        ),
        body: Center(
          child: Text(
            displaText,
            style: TextStyle(fontSize: 45),
          ),
        ),
      )
    );
  }
}
// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed:(){},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//             "Hello Flutter",
//             style: TextStyle(fontSize: 28),
//           ),
//         ),
//       )
//     );
//   }
// }


